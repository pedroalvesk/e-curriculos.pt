#!/bin/bash

##############################
# environment variables
##############################

# validate
if [ "$1" = "" ]
then
  echo "Usage: $0 <your-domain.com>"
  exit
fi

# fill in nginx.conf
domain="$1"
# shellcheck disable=SC2016
key_to_replace='#YOUR_DOMAIN_HERE#'

cp nginx.template nginx.conf
sed -i "s/$key_to_replace/$domain/g" nginx.conf


##############################
# install nginx
##############################

# update packages
yum update -y

# enable the EPEL repo
amazon-linux-extras enable epel

# install the EPEL repo
yum install -y epel-release

# install nginx
yum install -y nginx


##############################
# setup nginx
##############################

# copy config
cp nginx.conf /etc/nginx/nginx.conf

# permissions (security?)
chmod o+x /home/ec2-user/

# restart
systemctl restart nginx.service
